# Paramètres MAME:
# -rp <path> : Répertoire vers le dossier des ROMs, spécifié par index.json
# -nofilter : Désactive les filtres de redimensionnement (aspect pixelisé)
# -skip_gameinfo : Désactive le dialogue d’information sur les jeux
